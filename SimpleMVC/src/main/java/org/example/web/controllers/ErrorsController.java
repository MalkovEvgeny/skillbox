package org.example.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Evgeny Malkov
 */

@ControllerAdvice
public class ErrorsController {

  @GetMapping("/404")
  public String notFoundError(){
    return "404";
  }
}
