package org.example.web.controllers;

import javax.validation.constraints.NotEmpty;

import org.apache.log4j.Logger;
import org.example.app.services.BookService;
import org.example.web.dto.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/books")
@Scope("prototype")
public class BooksShelfController {

    private BookService bookService;

    @Autowired
    public BooksShelfController(BookService bookService) {
        this.bookService = bookService;
    }


    @PostMapping("/filtrationByAttribute")
    public String filter(@RequestParam(value = "bookAttributeToFiltration") String bookAttributeToFiltration, Model model){
        model.addAttribute("book", new Book());
        model.addAttribute("bookList", bookService.getFilteredBooks(bookAttributeToFiltration));
        return "book_shelf";
    }

    @GetMapping("/shelf")
    public String books(Model model){
        System.out.println(this.toString());
        model.addAttribute("book", new Book());
        model.addAttribute("bookList", bookService.getAllBooks());

        return "book_shelf";
    }



    @PostMapping("/save")
    public String saveBook(Book book){
        bookService.saveBook(book);
        return "redirect:/books/shelf";
    }

    @PostMapping("/removeByAttribute")
    public String remover(@RequestParam(value = "bookAttributeToRemove") String bookAttributeToRemove){
        bookService.removeByAttribute(bookAttributeToRemove);
        return "redirect:/books/shelf";
    }




}
