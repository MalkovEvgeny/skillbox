package org.example.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

/**
 * @author Evgeny Malkov
 */

@Configuration
@ComponentScan(basePackages = "org.example.web")
@EnableWebMvc
public class WebContextConfig implements WebMvcConfigurer {
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/**").addResourceLocations("classpath:images");
  }

  @Bean
  public SpringResourceTemplateResolver templateResolver(){
    SpringResourceTemplateResolver tempResolver = new SpringResourceTemplateResolver();
     tempResolver.setPrefix("WEB-INF/views/");
     tempResolver.setSuffix(".html");
     tempResolver.setTemplateMode("HTML5");

     return tempResolver;
  }

  @Bean
  public SpringTemplateEngine templateEngine(){
    SpringTemplateEngine templateEngine = new SpringTemplateEngine();
    templateEngine.setTemplateResolver(templateResolver());

    return templateEngine;
  }

  @Bean
  public ThymeleafViewResolver thymeleafViewResolver(){
    ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
    viewResolver.setTemplateEngine(templateEngine());
    viewResolver.setOrder(1);

    return viewResolver;
  }
}
