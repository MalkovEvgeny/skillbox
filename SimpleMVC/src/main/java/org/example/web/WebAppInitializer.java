package org.example.web;

import java.awt.event.ComponentListener;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.example.web.config.WebContextConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * @author Evgeny Malkov
 */
public class WebAppInitializer implements WebApplicationInitializer {
  @Override
  public void onStartup(javax.servlet.ServletContext servletContext) throws ServletException {
    System.out.println("loading app");
    XmlWebApplicationContext applicationContext = new XmlWebApplicationContext();
    applicationContext.setConfigLocation("classpath:app-config.xml");
    servletContext.addListener(new ContextLoaderListener(applicationContext));

    System.out.println("loading web");
    AnnotationConfigWebApplicationContext webApplicationContext = new AnnotationConfigWebApplicationContext();
    webApplicationContext.register(WebContextConfig.class);
    DispatcherServlet dispatcherServlet = new DispatcherServlet(webApplicationContext);

    ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher", dispatcherServlet);
    dispatcher.setLoadOnStartup(1);
    dispatcher.addMapping("/");

  }
}
