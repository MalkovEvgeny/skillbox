package org.example.app.services;

import org.example.web.dto.User;

public interface UsersRepository<T> {
    T saveNewUser(T user);
    boolean isExistUser(User user);
    boolean checkPassword(User user);
    T findUser(T user);
}
