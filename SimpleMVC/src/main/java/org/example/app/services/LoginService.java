package org.example.app.services;

import org.example.web.dto.User;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
    UsersRepository<User> userRep = new UsersRepositoryListImpl();

    public boolean authenticate(User user) {
        if ((user.getUsername() != null && !user.getUsername().trim().isEmpty()
                || user.getPassword() != null && !user.getPassword().trim().isEmpty())){
            if (userRep.isExistUser(user)) {
                System.out.println("Пользователь существует");
                if (userRep.checkPassword(user)) {
                    System.out.println("Пароль верный");
                    return true;
                }
            }
        }
        System.out.println("Пользователь не существует или пароль неверный");
        return false;
    }
}
