package org.example.app.services;

import org.example.web.dto.Book;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Repository
public class BookRepository implements ProjectRepository<Book> {

    private final List<Book> repo = new ArrayList<>();

    @Override
    public List<Book> filterRetrieve(String bookAttribute) {
        String actualPattern = checkAttributeForPattern(bookAttribute);
        Pattern pattern = Pattern.compile(actualPattern);
        Matcher matcher = pattern.matcher(bookAttribute);
        StringBuffer sb = new StringBuffer();

        while (matcher.find()) {
            matcher.appendReplacement(sb, "");
        }

        matcher.appendTail(sb);

        if (actualPattern.equalsIgnoreCase("id ")){
            int id = Integer.parseInt(sb.toString());
            return retrieveById(id);
        } else if (actualPattern.equalsIgnoreCase("size ")){
            int size = Integer.parseInt(sb.toString());
            return retrieveBySize(size);
        } else if (actualPattern.equalsIgnoreCase("Title ")){
            return retrieveByTitle(sb.toString());
        } else if (actualPattern.equalsIgnoreCase("Author ")){
            return retrieveByAuthor(sb.toString());
        } else {
            return retrieveAll();
        }
    }

    private List<Book> retrieveBySize(int size) {
        List<Book> books = new ArrayList<>();
        for (Book book : retrieveAll()) {
            if (book.getSize().equals(size)) {
                books.add(book);
            }
        }
        return books;
    }

    private List<Book> retrieveById(int id) {
        List<Book> books = new ArrayList<>();
        for (Book book : retrieveAll()) {
            if (book.getId().equals(id)) {
                books.add(book);
            }
        }
        return books;
    }

    private List<Book> retrieveByAuthor(String author) {
        List<Book> books = new ArrayList<>();
        for (Book book : retrieveAll()) {
            if (book.getAuthor().equalsIgnoreCase(author)) {
                books.add(book);
            }
        }
        return books;
    }

    private List<Book> retrieveByTitle(String title) {
        List<Book> books = new ArrayList<>();
        for (Book book : retrieveAll()) {
            if (book.getTitle().equalsIgnoreCase(title)) {
                books.add(book);
            }
        }
        return books;
    }

    @Override
    public List<Book> retrieveAll() {
        return new ArrayList<>(repo);
    }


    @Override
    public void store(Book book) {
        if ((book.getAuthor() != null && !book.getAuthor().trim().isEmpty()
                || book.getTitle() != null && !book.getTitle().trim().isEmpty()
                || book.getSize() != null)) {
            book.setId(book.hashCode());
            repo.add(book);
        }
    }

    @Override
    public boolean removeItemByAttribute(String bookAttribute) {
        String actualPattern = checkAttributeForPattern(bookAttribute);
        Pattern pattern = Pattern.compile(actualPattern);
        Matcher matcher = pattern.matcher(bookAttribute);
        StringBuffer sb = new StringBuffer();

        while (matcher.find()) {
            matcher.appendReplacement(sb, "");
        }

        matcher.appendTail(sb);

        if (actualPattern.equalsIgnoreCase("id ")){
            int id = Integer.parseInt(sb.toString());
            removeItemById(id);
            return true;
        } else if (actualPattern.equalsIgnoreCase("size ")){
            int size = Integer.parseInt(sb.toString());
            removeItemBySize(size);
            return true;
        } else if (actualPattern.equalsIgnoreCase("Title ")){
            removeItemByTitle(sb.toString());
            return true;
        } else if (actualPattern.equalsIgnoreCase("Author ")){
            removeItemByAuthor(sb.toString());
            return true;
        }else{
            return false;
        }
    }




    public String checkAttributeForPattern(String bookAttribute) {
        String[] patterns = new String[]{"id ", "Title ", "Author ", "Size "};
        for (String pattern : patterns) {
            if (bookAttribute.contains(pattern)) {
                return pattern;
            }
        }
        return "Other";
    }

    @Override
    public boolean removeItemBySize(Integer bookSizeRemove) {
        for (Book book : retrieveAll()) {
            if (book.getSize().equals(bookSizeRemove)) {
                repo.remove(book);
            }
        }
        return false;
    }

    @Override
    public boolean removeItemById(Integer bookIdRemove) {
        for (Book book : retrieveAll()) {
            if (book.getId().equals(bookIdRemove)) {
                repo.remove(book);
            }
        }
        return false;
    }

    @Override
    public boolean removeItemByAuthor(String bookAuthorRemove) {
        for (Book book : retrieveAll()) {
            if (book.getAuthor().equalsIgnoreCase(bookAuthorRemove)) {
                repo.remove(book);
            }
        }
        return false;
    }

    @Override
    public boolean removeItemByTitle(String bookTitleRemove) {
        for (Book book : retrieveAll()) {
            if (book.getTitle().equalsIgnoreCase(bookTitleRemove)) {
                repo.remove(book);
            }
        }
        return false;
    }


}
