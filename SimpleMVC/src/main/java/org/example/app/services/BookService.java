package org.example.app.services;

import org.example.web.dto.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    private final ProjectRepository<Book> bookRepo;

    @Autowired
    public BookService(ProjectRepository<Book> bookRepo) {
        this.bookRepo = bookRepo;
    }

    public List<Book> getFilteredBooks(String bookAttribute){
        return bookRepo.filterRetrieve(bookAttribute);
    }

    public List<Book> getAllBooks(){
        return bookRepo.retrieveAll();
    }

    public void saveBook(Book book) {
        bookRepo.store(book);
    }

    public boolean removeBookById(Integer bookIdRemove) {
        return bookRepo.removeItemById(bookIdRemove);
    }

    public boolean removeBookBySize(Integer bookSizeRemove) {
        return bookRepo.removeItemBySize(bookSizeRemove);
    }

    public boolean removeBookByAuthor(String bookAuthorRemove) {
        return bookRepo.removeItemByAuthor(bookAuthorRemove);
    }

    public boolean removeBookByTitle(String bookTitleRemove) {
        return bookRepo.removeItemByTitle(bookTitleRemove);
    }

    public boolean removeByAttribute(String bookAttribute){
        return bookRepo.removeItemByAttribute(bookAttribute);
    }

}
