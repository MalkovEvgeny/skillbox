package org.example.app.services;

import org.example.web.dto.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class UsersRepositoryListImpl implements UsersRepository<User> {

    private final List<User> repo = new ArrayList<>();


    @Override
    public User saveNewUser(User user) {
        repo.add(user);
        return user;
    }

    @Override
    public boolean isExistUser(User user) {
        for (int i = 0; i < repo.size(); i++){
            System.out.println(repo.get(i).getUsername());
            System.out.println(repo.get(i).getPassword());
            if (repo.get(i).getUsername().equals(user.getUsername())){
                return true;
            }
        }
        saveNewUser(user);
        return false;
    }


    @Override
    public boolean checkPassword(User user) {
        for (int i = 0; i < repo.size(); i++) {
            if (repo.get(i).getPassword().equals(user.getPassword())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public User findUser(User user) {
        for (int i = 0; i < repo.size(); i++) {
            if (repo.get(i).getUsername().equals(user.getUsername())) {
                System.out.println("Пользователь найден");
                return user;
            }
        }
        return null;
    }
}
