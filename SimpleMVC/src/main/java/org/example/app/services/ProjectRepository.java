package org.example.app.services;

import java.util.List;

public interface ProjectRepository<T> {
    List<T> retrieveAll();

    void store(T book);

    boolean removeItemById(Integer bookIdRemove);

    boolean removeItemBySize(Integer bookSizeRemove);

    boolean removeItemByAuthor(String bookAuthorRemove);

    boolean removeItemByTitle(String bookTitleRemove);

    boolean removeItemByAttribute(String bookAttribute);

    List<T> filterRetrieve(String bookAttribute);
}
