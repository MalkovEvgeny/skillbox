package org.example.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Evgeny Malkov
 */
@Configuration
@ComponentScan(basePackages = "org.example.app")
public class AppContextConfig {

  @Bean
  public IdProvider

}
